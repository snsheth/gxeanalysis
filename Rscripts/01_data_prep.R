#### PROJECT: Chamaecrista fasciculata gxe study (2015 census data from Grey Cloud & Flint Hills)
#### PURPOSE: Clean data and combine data from both sites into one file
#### AUTHOR: Rachel Pain, Mason Kulbaba & Seema Sheth
#### DATE LAST MODIFIED: 20180703

# load packages
library(ggplot2)

# Read in 2015 census data for Flint Hills and Grey Cloud
data.FH <-read.csv("Data/FH_2015_census_data_revised.csv") 
data.GC<-read.csv("Data/GC_2015_census_data_revised.csv") 

#**********************************************************************************
# 1. CLEAN DATA
#**********************************************************************************

#**********************************************************************************
# 1A. FLINT HILLS
#**********************************************************************************

#**********************************************************************************
# Examine positions where maternal &/or paternal ID are NA (missing)
subset(data.FH,is.na(maternalID)|is.na(paternalID)) # all of these positions are places where a seed was not planted

# Remove rows where maternal & paternal ID are NA (missing)
data.FH=subset(data.FH,!is.na(maternalID)&!is.na(paternalID))

#**********************************************************************************
# Convert NAs in data to 0
data.FH[is.na(data.FH)]<-0

# Check that NAs have been converted to zero
head(data.FH)

#**********************************************************************************
# Create Additional columnn with date (census #) of first flower
data.FH$flower.date=ifelse(data.FH$flowering4==1,4,ifelse(data.FH$flowering5==1,5,
                                                          ifelse(data.FH$flowering8==1,8,ifelse(data.FH$flowering8==0,0,0))))

#********************************************************************************
# Find detection errors in data for presence columns
subset(data.FH,presence1==1&presence2==0&presence3==1) # 3 errors
subset(data.FH,presence2==1&presence3==0&presence4==1) # 1 error
subset(data.FH,presence3==1&presence4==0&presence5==1) # 1 error
subset(data.FH,presence4==1&presence5==0&presence6==1) # no errors
subset(data.FH,presence5==1&presence6==0&presence8==1) # 1 error

# In some cases plants were marked as present in one census, not present in the next census, and then present again in the following census.
## We want to clean these errors so that individuals are consistently present once germinated and before dying
data.FH$presence2[data.FH$presence1==1&data.FH$presence3==1]=1
data.FH$presence3[data.FH$presence2==1&data.FH$presence4==1]=1
data.FH$presence4[data.FH$presence3==1&data.FH$presence5==1]=1
data.FH$presence5[data.FH$presence4==1&data.FH$presence6==1]=1
data.FH$presence6[data.FH$presence5==1&data.FH$presence8==1]=1

# Verify that detection errors in data for presence columns are fixed
subset(data.FH,presence1==1&presence2==0&presence3==1) # no errors
subset(data.FH,presence2==1&presence3==0&presence4==1) # no errors
subset(data.FH,presence3==1&presence4==0&presence5==1) # no errors
subset(data.FH,presence4==1&presence5==0&presence6==1) # no errors
subset(data.FH,presence5==1&presence6==0&presence8==1) # no errors

#*******************************************************************************
# Find detection errors in flowering, once a plant has flowered it stays flowering until dead
subset(data.FH,flowering4==1&flowering5==0&flowering8==1) # 1 error
subset(data.FH,presence8==1&flowering8==0&flowering5==1) # no errors

# Fix detection errors in flowering
data.FH$flowering5[data.FH$flowering4==1&data.FH$flowering8==1]=1
data.FH$flowering8[data.FH$presence8==1&data.FH$flowering5==1]=1

# Veriy that errors hav been fixed
subset(data.FH,flowering4==1&flowering5==0&flowering8==1) # no errors
subset(data.FH,presence8==1&flowering8==0&flowering5==1) # no errors

#*******************************************************************************

# Create Additional variable (called flw) if plant flowered at all (0= no, 1 = yes)
data.FH$flw=ifelse(data.FH$flower.date > 0, 1,0)

# Subset data with plants that were present at the first (census 1) and last (census 8) census
ends<- subset(data.FH, presence1==1&presence8==1)

# detect plants with missing presence data between the first (census 1) and last (census 8) census
zeros<- subset(ends, presence2==0|presence3==0|presence4==0|presence5==0|presence6==0)#1 plant this condition

# fill in ones for all plants with presence1=1 and presence8=1
data.FH$presence2[data.FH$presence1==1&data.FH$presence8==1]= 1
data.FH$presence3[data.FH$presence1==1&data.FH$presence8==1]= 1
data.FH$presence4[data.FH$presence1==1&data.FH$presence8==1]= 1
data.FH$presence5[data.FH$presence1==1&data.FH$presence8==1]= 1
data.FH$presence6[data.FH$presence1==1&data.FH$presence8==1]= 1

# check for correction
ends<- subset(data.FH, presence1==1&presence8==1)

# detect plants with missing presence data between the first (census 1) and last (census 8) census
subset(ends, presence2==0|presence3==0|presence4==0|presence5==0|presence6==0)#no plants meet this criteria.  Correction complete.

# Make "block" variable from "row"
# e.g. take first digit from row and make it into Block
data.FH$block <- substr(data.FH$row,1,1)

# make variable if plant germinated at all (from presence1 to presence8)? 1= yes, 0= no
data.FH$Germ=ifelse(data.FH$presence1|data.FH$presence2|data.FH$presence3|data.FH$presence4|data.FH$presence5|data.FH$presence6 >0, 1, 0)

# create variable of total number of pods per plant
data.FH$total.pods= data.FH$green.pods8+data.FH$ground.pods8+data.FH$crispy.pods8 + data.FH$pods.on.plant8

# add site variable for Flint Hills site (away) 
data.FH$site<- "B"

# compute mean number of seeds per fruit
mean(data.FH$seed.ct/data.FH$crispy.pods8,na.rm=TRUE)
mean(data.FH$green.seed.ct/data.FH$green.pods8,na.rm=TRUE)
mean(data.FH$ground.seed.ct/data.FH$ground.pods8,na.rm=TRUE)
data.FH$seeds_per_fruit=data.FH$seed.ct/data.FH$crispy.pods8

# obtain standard error of mean # of seeds per fruit
sd(data.FH$seeds_per_fruit[!is.na(data.FH$seeds_per_fruit)])/sqrt(length(data.FH$seeds_per_fruit[!is.na(data.FH$seeds_per_fruit)])) 

# create data frame with relevant columns for aster
fh=subset(data.FH,select=c("row","position","maternalID","paternalID","flw","block","Germ","total.pods","site"))

# inspect sample sizes at Flint Hills
length(fh$row) # total number of seeds planted: 3,569
length(unique(fh$maternalID)) # 121 dams
length(unique(fh$paternalID)) # 42 sires

# tally germination, flowering, and fruit production 
length(fh$Germ[fh$Germ==1]) # 564 plants germinated
length(fh$Germ[fh$flw==1]) # 418 plants flowered
length(fh$total.pods[fh$total.pods>0]) # 363 plants produced at least one pod (fruit)
summary(fh$total.pods[fh$total.pods>0]) # obtain range and mean fruit #
sd(fh$total.pods[fh$total.pods>0])/sqrt(length(fh$total.pods[fh$total.pods>0])) # obtain standard error of mean fruit #

#**********************************************************************************
# 1B. GREY CLOUD
#**********************************************************************************

#**********************************************************************************
# Examine positions where maternal &/or paternal ID are NA (missing)
subset(data.GC,is.na(maternalID)|is.na(paternalID)) # all of these positions are places where a seed was not planted

# Remove rows where maternal & paternal ID are NA (missing)
data.GC=subset(data.GC,!is.na(maternalID)&!is.na(paternalID))

#**********************************************************************************
# Convert NAs in data to 0
data.GC[is.na(data.GC)]<-0

# Create Additional columnn with date (census #) of first flower
data.GC$flower.date=ifelse(data.GC$flowering4==1,4,ifelse(data.GC$flowering5==1,5,
                                                          ifelse(data.GC$flowering9==1,9,ifelse(data.GC$flowering9==0,0,0))))
#**********************************************************************************
# Find detection errors in data for presence columns (presence 1 to presence 9)
subset(data.GC,presence1==1&presence2==0&presence3==1) # 2 errors
subset(data.GC,presence2==1&presence3==0&presence4==1) # 4 errors
subset(data.GC,presence3==1&presence4==0&presence5==1) # no errors
subset(data.GC,presence4==1&presence5==0&presence6==1) # 5 errors
subset(data.GC,presence5==1&presence6==0&presence7==1) # no errors
subset(data.GC,presence6==1&presence7==0&presence9==1) # 2 errors

## In some cases plants were marked as present in one census, not present in the next census, and then present again in the following census.
### We want to clean these errors so that individuals are consistently present once germinated and before dying
data.GC$presence2[data.GC$presence1==1&data.GC$presence3==1]=1
data.GC$presence3[data.GC$presence2==1&data.GC$presence4==1]=1
data.GC$presence4[data.GC$presence3==1&data.GC$presence5==1]=1
data.GC$presence5[data.GC$presence4==1&data.GC$presence6==1]=1
data.GC$presence6[data.GC$presence5==1&data.GC$presence7==1]=1
data.GC$presence7[data.GC$presence6==1&data.GC$presence9==1]=1

# Verify that all errors in presence are fixed
subset(data.GC,presence1==1&presence2==0&presence3==1) # no errors
subset(data.GC,presence2==1&presence3==0&presence4==1) # no errors
subset(data.GC,presence3==1&presence4==0&presence5==1) # no errors
subset(data.GC,presence4==1&presence5==0&presence6==1) # no errors
subset(data.GC,presence5==1&presence6==0&presence7==1) # no errors
subset(data.GC,presence6==1&presence7==0&presence9==1) # no errors

# Find detection errors in data for flowering columns
## Once a plant has flowered it stays flowering until death
subset(data.GC,flowering4==1&flowering5==0&flowering9==1) # no errors
subset(data.GC,presence9==1&flowering9==0&flowering5==1) # 3 errors

# Fix errors in flowering columns
data.GC$flowering5[data.GC$flowering4==1&data.GC$flowering9==1]=1
data.GC$flowering9[data.GC$presence9==1&data.GC$flowering5==1]=1

# Verify that errors in flowering have been fixed
subset(data.GC,flowering4==1&flowering5==0&flowering9==1) # fixed
subset(data.GC,presence9==1&flowering9==0&flowering5==1) # fixed

#**********************************************************************************

# Create Additional variable (called flw) if plant flowered at all (0= no, 1 = yes)
data.GC$flw=ifelse(data.GC$flower.date > 0, 1,0)

## subset plants that were present at the first (presence1) and last (presence9) census
ends<- subset(data.GC, presence1==1&presence9==1)

# identify detection errors for plants that were present between the first (presence1) and last (presence9) census
zeros<- subset(ends, presence2==0|presence3==0|presence4==0|presence5==0|presence6==0|presence7==0)#10 plants have this condition

# fill in ones for all planTs with p1=1 and p9=1, were munched, but did flower later
data.GC$presence2[data.GC$presence1==1&data.GC$presence9==1]= 1
data.GC$presence3[data.GC$presence1==1&data.GC$presence9==1]= 1
data.GC$presence4[data.GC$presence1==1&data.GC$presence9==1]= 1
data.GC$presence5[data.GC$presence1==1&data.GC$presence9==1]= 1
data.GC$presence6[data.GC$presence1==1&data.GC$presence9==1]= 1
data.GC$presence7[data.GC$presence1==1&data.GC$presence9==1]= 1

# check for correction
ends<- subset(data.GC, presence1==1&presence9==1)

# plants with a zero between presence1 and presence9
subset(ends, presence2==0|presence3==0|presence4==0|presence5==0|presence6==0|presence7==0) #no plants meet this criteria. Correction complete.

#**********************************************************************************
# 2. PREPARE DATA FOR ASTER
#**********************************************************************************

# make new variable Site for each location FH and GC
data.GC$site<- "A"

# make block varible from first digit of row number
data.GC$block <- substr(data.GC$row,1,1)

# add site prefix to each block ID
data.GC$block <- sub("^", "A", data.GC$block)
fh$block <- sub("^", "B", data.FH$block)

# add total.pods to GC data, but remove "pods on plants" column if using total seed counts in aster models
data.GC$total.pods<- data.GC$ground.pods9 + data.GC$green.pods9 + data.GC$crispy.pods9 + data.GC$pods.on.plant9

# Make variable Germ: did seed germinate at all (0=no, 1=yes)?
data.GC$Germ=ifelse(data.GC$presence1|data.GC$presence2|data.GC$presence3|data.GC$presence4|data.GC$presence5|data.GC$presence6|data.GC$presence7
                  |data.GC$presence9 >0, 1, 0)

# compute mean number of seeds per fruit
mean(data.GC$seed.ct/data.GC$crispy.pods9,na.rm=TRUE)
mean(data.GC$green.seed.ct/data.GC$green.pods9,na.rm=TRUE)
mean(data.GC$ground.seed.ct/data.GC$ground.pods9,na.rm=TRUE)
data.GC$seeds_per_fruit=data.GC$seed.ct/data.GC$crispy.pods9

# obtain standard error of mean # of seeds per fruit
sd(data.GC$seeds_per_fruit[!is.na(data.GC$seeds_per_fruit)])/sqrt(length(data.GC$seeds_per_fruit[!is.na(data.GC$seeds_per_fruit)])) 

# combine seeds per fruit from FH & GC to address reviewer comments
seeds_per_fruit.GC=data.frame(data.GC$seeds_per_fruit[!is.na(data.GC$seeds_per_fruit)],data.GC$site[!is.na(data.GC$seeds_per_fruit)])
colnames(seeds_per_fruit.GC)=c("seeds_per_fruit","site")
seeds_per_fruit.FH=data.frame(data.FH$seeds_per_fruit[!is.na(data.FH$seeds_per_fruit)],data.FH$site[!is.na(data.FH$seeds_per_fruit)])
colnames(seeds_per_fruit.FH)=c("seeds_per_fruit","site")
seeds_per_fruit.GCFH=rbind(seeds_per_fruit.GC,seeds_per_fruit.FH)
seeds_per_fruit.GCFH$site=ifelse(seeds_per_fruit.GCFH$site=="A","GC","FH")

# set plotting theme for ggplot
theme_set(theme_minimal(base_size = 22))

# plots seeds per fruit for each site
ggplot(seeds_per_fruit.GCFH, aes(x=site, y=seeds_per_fruit)) + 
  geom_boxplot() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  stat_summary(fun.y=mean, geom="point", shape=23, size=6,fill="darkgreen",alpha=0.5) +
  xlab("Site") +
  ylab("# seeds per fruit") 
t.test(seeds_per_fruit.GCFH$seeds_per_fruit~seeds_per_fruit.GCFH$site)

# pick final variables for GC site: row, position, maternalID, paternalID,flw,site, block, total.pods, Germ
gcd<-subset(data.GC,select=c("row","position","maternalID","paternalID","flw","block","Germ","total.pods","site"))

# inspect sample sizes at Grey Cloud
length(gcd$row) # total # of seeds planted: 3,658
length(unique(gcd$maternalID)) # 123 dams
length(unique(gcd$paternalID)) # 42 sires

# tally germination, flowering, and fruit production 
length(gcd$Germ[gcd$Germ==1]) # 721 plants germinated
length(gcd$Germ[gcd$flw==1]) # 621 plants flowered
length(gcd$total.pods[gcd$total.pods>0]) # 336 plants produced at least one pod (fruit)
summary(gcd$total.pods[gcd$total.pods>0]) # obtain range and mean fruit #
sd(gcd$total.pods[gcd$total.pods>0])/sqrt(length(gcd$total.pods[gcd$total.pods>0])) # obtain standard error of mean fruit #

# combine the data from both sites into a single file
merg<- rbind(gcd, fh)

# check if any prods produced when flw = 0
test<- subset(merg, flw==1 & total.pods==0) # 340 plants flowered but did not produce pods (fruit)

# write combined data frame to .csv file
write.csv(merg,"Routput/GC_FH_2015_census_data_cleaned.csv",row.names = FALSE)